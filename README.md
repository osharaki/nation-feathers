## Nation Feathers
Nation Feathers is a top-down/3rd-person-view, endless flying mobile game for
Android.
The game is developed in Unity using C#.
More on the technical details is provided in the pdf (German).
Feel free to download the .apk file and install the game on your
device.

## Plot
You are a hungry, aquatic bird on a mission; to eat as many squid floating on the 
water as you can before you starve.
But be careful! The water level is dropping rapidly, and with it the floating 
squid. Navigate your way through the swampy terrain and avoid bumping into the 
hills. Bon Apetit!